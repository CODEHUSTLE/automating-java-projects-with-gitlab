package org.hsmith.medium;

final class WorkerClass {
    int sum(final int value0, final int value1) {
        return value0 + value1;
    }
}
